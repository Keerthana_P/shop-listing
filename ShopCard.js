import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center'
    },
    heading : {
        fontSize: 20 ,
        marginLeft : 60,
        marginTop : 50,
        marginBottom : 10 ,
        justifyContent : "center",
        fontWeight :"bold"
    },
    scrollView: {
        marginHorizontal: 10,
    },
    scrollContent : {
        flexDirection: 'column',
        marginLeft : 10
    },
    scrollViewLayout : {
        flexDirection: "row" ,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop : 50
    },
    image: {
        width: "30%",
        height: "100%",
        aspectRatio: 2
    },
    marginl : {
        marginLeft: 20
    },
    b_image : {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        opacity: 0.8
    },
    title : {
        fontSize: 14,
        fontWeight :"bold"
    },
    body : {
        fontSize: 12 ,
        justifyContent : "center"
    },
    price : {
        flexDirection: 'row' ,
        justifyContent: 'space-between'
    },
    priceLayoutFonts : {
        fontSize: 10
    },
    button : {
        justifyContent : "center"
    }
});