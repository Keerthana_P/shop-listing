import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import data from "./data.json";
import registerRootComponent from 'expo';
import styles from "./ShopCard.js";
import { ImageBackground , StyleSheet, Text, View , ScrollView , Image , AppRegistry , FlatList , TouchableOpacity , Button } from 'react-native';

const b_img = { uri: "https://png.pngtree.com/thumb_back/fh260/back_pic/03/57/60/3257a153d12cde5.jpg" };


export default class App extends Component {
    // noinspection JSAnnotator
    constructor(props:TodoProps){
        super(props);
        this.state = {
            todoText:" "
        };
    }
    onPressButton = (value) => {
        alert(value);
    }
  render() {
  return (
      <ScrollView  style={styles.scrollView}>
        <View style={{flex: 1}}>
          <ImageBackground source={b_img} style={styles.b_image}>
          <Text style={styles.heading}>Shops Around You..</Text>
              <FlatList
                  data={data}
                  renderItem={({item}) =>
                      (
                          // data.map((postData) => {
                          <TouchableOpacity  style={styles.button} onPress = {()=>this.onPressButton(item.title)} >
                          <View style={styles.scrollViewLayout} key={item.id}>
                              <Image style={styles.image}  source={{ uri: item.image }} />
                              <View style={styles.scrollContent}>
                                  <Text  style={styles.title}>
                                      {item.title}
                                  </Text>
                                  <Text numberOfLines={1} style={styles.body}>
                                      {item.body > 100
                                          ? `${item.body}`
                                          : `${item.body.substring(0, 36)}...`}
                                  </Text>
                                  <View  style={styles.price}><Text style={styles.priceLayoutFonts}>{item.time}</Text>
                                      <Text style={styles.priceLayoutFonts}>{item.price}</Text>
                                  </View>
                              </View>
                          </View>
                          </TouchableOpacity>)}
              />
          </ImageBackground>
          </View>
      </ScrollView>
);
}}

